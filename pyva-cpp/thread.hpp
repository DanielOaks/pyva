#ifndef WIN32
#include <pthread.h>
#else
#include <windows.h>
#endif

namespace Thread
{
	class Mutex
	{
#ifndef WIN32
		pthread_mutex_t mutex;
#else
		CRITICAL_SECTION mutex;
#endif

	 public:
		Mutex();
		~Mutex();

		void Lock();
		void Unlock();
	};

	struct Lock
	{
		Mutex &mutex;

		Lock(Mutex &m);
		~Lock();
	};
}

