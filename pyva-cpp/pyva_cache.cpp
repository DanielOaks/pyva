#include "python.hpp"

Pyva::HashNode::HashNode(int hv, jobject r, PyObject *p) : hashv(hv), ref(r), strong(true), py(p)
{
}

static void invalidate(JNIEnv *env, Pyva::HashNode &h)
{
	/* this must be a weak reference that is now invalidated */
	assert(!h.strong);

	/* we must be the only thing with a reference to this Python object */
	assert(h.py->ob_refcnt == 1);

	/* both objects go away */
	Py_DECREF(h.py);
	env->DeleteWeakGlobalRef(h.ref);
}

static bool swap_to_weak(JNIEnv *env, Pyva::HashNode &h)
{
	/* this must be a strong reference that is being changed to weak */
	assert(h.strong);

	/* we must be the only thing with a reference to this Python object */
	assert(h.py->ob_refcnt == 1);

	jobject ref = h.ref,
		wref = env->NewWeakGlobalRef(ref);

	if (wref == nullptr)
		return false;

	h.ref = wref;
	env->DeleteGlobalRef(ref);

	h.strong = false;

	return true;
}

PyObject *Pyva::IsCached(jobject jobj)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	assert(jobj != nullptr);

	int hashv = JNI::Hash(jobj);
	unsigned int bucketv = hashv & (buckets - 1);

	Bucket &bucket = objects[bucketv];
	for (auto it = bucket.begin(); it != bucket.end();)
	{
		Pyva::HashNode &h = *it;

		/* h.ref can be weak so take a local reference to it first */
		JNI::LocalReference<jobject> ref = env->NewLocalRef(h.ref);

		if (ref == nullptr)
		{
			/* invalidate */
			invalidate(env, h);

			/* hash entry goes away */
			it = bucket.erase(it);
		}
		else if (hashv == h.hashv && env->IsSameObject(h.ref, jobj))
		{
			/* this is the object we are looking for */
			if (!h.strong)
			{
				/* we've looked up an object we are holding a weak reference to.
				 * since we're going to be passing this Python object around a bit now
				 * convert it to a global reference so it isn't GC'd out from under us
				 */

				/* if this is weak nothing else must have touched it */
				assert(h.py->ob_refcnt == 1);

				jobject ref = h.ref,
					sref = env->NewGlobalRef(ref);

				if (sref == nullptr)
					/* this has already thrown; rv doesn't matter */
					break;

				/* swap */
				h.ref = sref;
				env->DeleteWeakGlobalRef(ref);

				h.strong = true;
			}

			return h.py;
		}
		else if (h.py->ob_refcnt == 1 && h.strong)
		{
			/* swap to weak reference as we are the only thing holding a reference to this Python object */
			if (!swap_to_weak(env, h))
				break;

			++it;
		}
		else
		{
			++it;
		}
	}

	return nullptr;
}

bool Pyva::IsStrong(jobject obj) const
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	int hashv = JNI::Hash(obj);
	unsigned int bucketv = hashv & (buckets - 1);
	const Bucket &bucket = objects[bucketv];

	for (const HashNode &h : bucket)
		if (hashv == h.hashv && env->IsSameObject(h.ref, obj))
			return h.strong;

	return false;
}

void Pyva::Cache(jobject jobj, PyObject *pyobject)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	assert(jobj != nullptr);
	assert(pyobject != nullptr);

	jobject strongref = env->NewGlobalRef(jobj);
	if (strongref == nullptr)
		return;

	Py_INCREF(pyobject);

	int hashv = JNI::Hash(jobj);
	unsigned int bucketv = hashv & (buckets - 1);

	Bucket &bucket = objects[bucketv];
	bucket.emplace_back(hashv, strongref, pyobject);
}

int Pyva::GC()
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	int count = 0;

	for (size_t i = 0; i < buckets; ++i)
		for (auto it = objects[i].begin(); it != objects[i].end();)
		{
			Pyva::HashNode &h = *it;

			/* h.ref can be weak so take a local reference to it first */
			JNI::LocalReference<jobject> ref = env->NewLocalRef(h.ref);

			if (ref == nullptr)
			{
				/* invalidate */
				invalidate(env, h);

				/* hash entry goes away */
				it = objects[i].erase(it);

				++count;
			}
			else if (h.py->ob_refcnt == 1 && h.strong)
			{
				/* swap to weak reference as we are the only thing holding a reference to this Python object */
				swap_to_weak(env, h);

				++it;
			}
			else
				++it;
		}

	return count;
}

void Pyva::Clear()
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	for (size_t i = 0; i < buckets; ++i)
		for (auto it = objects[i].begin(); it != objects[i].end();)
		{
			HashNode &h = *it;

			Py_DECREF(h.py);
			if (h.strong)
				env->DeleteGlobalRef(h.ref);
			else
				env->DeleteWeakGlobalRef(h.ref);

			it = objects[i].erase(it);
		}
}

