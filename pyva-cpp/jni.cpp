#include "python.hpp"

JavaVM *JNI::vm = nullptr;

JNI::StaticGlobalReference<jclass>
	JNI::java_lang_Object,
	JNI::java_lang_Boolean,
	JNI::java_lang_Integer,
	JNI::java_lang_Double,
	JNI::java_lang_Long,
	JNI::java_lang_String,
	JNI::java_lang_Throwable,
	JNI::net_rizon_pyva_PyvaException,
	JNI::net_rizon_pyva_Util;

jmethodID
	JNI::java_lang_Object_toString,
	JNI::java_lang_Object_hashCode,
	JNI::java_lang_Boolean_init,
	JNI::java_lang_Boolean_booleanValue,
	JNI::java_lang_Integer_init,
	JNI::java_lang_Integer_intValue,
	JNI::java_lang_Double_init,
	JNI::java_lang_Double_doubleValue,
	JNI::java_lang_Long_init,
	JNI::java_lang_Long_longValue,
	JNI::net_rizon_pyva_PyvaException_init,
	JNI::net_rizon_pyva_Util_isArray,
	JNI::net_rizon_pyva_Util_getIterator,
	JNI::net_rizon_pyva_Util_hasNextIterator,
	JNI::net_rizon_pyva_Util_nextIterator,
	JNI::net_rizon_pyva_Util_getClassName,
	JNI::net_rizon_pyva_Util_getAttribute,
	JNI::net_rizon_pyva_Util_setAttribute,
	JNI::net_rizon_pyva_Util_invokeConstructor,
	JNI::net_rizon_pyva_Util_invoke,
	JNI::net_rizon_pyva_Util_getCause,
	JNI::net_rizon_pyva_Util_splitException,
	JNI::net_rizon_pyva_Util_prettyException;

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved)
{
	JNI::vm = vm;

	JNI::Start();
	Python::Start();

	return JNI_VERSION_1_6;
}

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM *vm, void *reserved)
{
	Python::Stop();
	JNI::Stop();
}

void JNI::Start()
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	java_lang_Object = env->FindClass("java/lang/Object");
	java_lang_Boolean = env->FindClass("java/lang/Boolean");
	java_lang_Integer = env->FindClass("java/lang/Integer");
	java_lang_Double = env->FindClass("java/lang/Double");
	java_lang_Long = env->FindClass("java/lang/Long");
	java_lang_String = env->FindClass("java/lang/String");
	java_lang_Throwable = env->FindClass("java/lang/Throwable");
	net_rizon_pyva_PyvaException = env->FindClass("net/rizon/pyva/PyvaException");
	net_rizon_pyva_Util = env->FindClass("net/rizon/pyva/Util");

	assert(java_lang_Object);
	assert(java_lang_Boolean);
	assert(java_lang_Integer);
	assert(java_lang_Double);
	assert(java_lang_Long);
	assert(java_lang_String);
	assert(java_lang_Throwable);
	assert(net_rizon_pyva_PyvaException);
	assert(net_rizon_pyva_Util);

	java_lang_Object_toString = env->GetMethodID(java_lang_Object, "toString", "()Ljava/lang/String;");
	java_lang_Object_hashCode = env->GetMethodID(java_lang_Object, "hashCode", "()I");
	java_lang_Boolean_init = env->GetMethodID(java_lang_Boolean, "<init>", "(Z)V");
	java_lang_Boolean_booleanValue = env->GetMethodID(java_lang_Boolean, "booleanValue", "()Z");
	java_lang_Integer_init = env->GetMethodID(java_lang_Integer, "<init>", "(I)V");
	java_lang_Integer_intValue = env->GetMethodID(java_lang_Integer, "intValue", "()I");
	java_lang_Double_init = env->GetMethodID(java_lang_Double, "<init>", "(D)V");
	java_lang_Double_doubleValue = env->GetMethodID(java_lang_Double, "doubleValue", "()D");
	java_lang_Long_init = env->GetMethodID(java_lang_Long, "<init>", "(J)V");
	java_lang_Long_longValue = env->GetMethodID(java_lang_Long, "longValue", "()J");
	net_rizon_pyva_PyvaException_init = env->GetMethodID(net_rizon_pyva_PyvaException, "<init>", "(Ljava/lang/String;)V");
	net_rizon_pyva_Util_isArray = env->GetStaticMethodID(net_rizon_pyva_Util, "isArray", "(Ljava/lang/Object;)Z");
	net_rizon_pyva_Util_getIterator = env->GetStaticMethodID(net_rizon_pyva_Util, "getIterator", "(Ljava/lang/Object;)Ljava/util/Iterator;");
	net_rizon_pyva_Util_hasNextIterator = env->GetStaticMethodID(net_rizon_pyva_Util, "hasNextIterator", "(Ljava/util/Iterator;)Z");
	net_rizon_pyva_Util_nextIterator = env->GetStaticMethodID(net_rizon_pyva_Util, "nextIterator", "(Ljava/util/Iterator;)Ljava/lang/Object;");
	net_rizon_pyva_Util_getClassName = env->GetStaticMethodID(net_rizon_pyva_Util, "getClassName", "(Ljava/lang/Object;)Ljava/lang/String;");
	net_rizon_pyva_Util_getAttribute = env->GetStaticMethodID(net_rizon_pyva_Util, "getAttribute", "(Lnet/rizon/pyva/Pyva;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	net_rizon_pyva_Util_setAttribute = env->GetStaticMethodID(net_rizon_pyva_Util, "setAttribute", "(Lnet/rizon/pyva/Pyva;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z");
	net_rizon_pyva_Util_invokeConstructor = env->GetStaticMethodID(net_rizon_pyva_Util, "invokeConstructor", "(Lnet/rizon/pyva/Pyva;Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;");
	net_rizon_pyva_Util_invoke = env->GetStaticMethodID(net_rizon_pyva_Util, "invoke", "(Lnet/rizon/pyva/Pyva;Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;");
	net_rizon_pyva_Util_getCause = env->GetStaticMethodID(net_rizon_pyva_Util, "getCause", "(Ljava/lang/Throwable;)Ljava/lang/Throwable;");
	net_rizon_pyva_Util_splitException = env->GetStaticMethodID(net_rizon_pyva_Util, "splitException", "(Ljava/lang/Throwable;)[[Ljava/lang/Object;");
	net_rizon_pyva_Util_prettyException = env->GetStaticMethodID(net_rizon_pyva_Util, "prettyException", "(Ljava/lang/Throwable;)V");

	assert(java_lang_Object_toString);
	assert(java_lang_Object_hashCode);
	assert(java_lang_Boolean_init);
	assert(java_lang_Boolean_booleanValue);
	assert(java_lang_Integer_init);
	assert(java_lang_Integer_intValue);
	assert(java_lang_Double_init);
	assert(java_lang_Double_doubleValue);
	assert(java_lang_Long_init);
	assert(java_lang_Long_longValue);
	assert(net_rizon_pyva_PyvaException_init);
	assert(net_rizon_pyva_Util_isArray);
	assert(net_rizon_pyva_Util_getIterator);
	assert(net_rizon_pyva_Util_hasNextIterator);
	assert(net_rizon_pyva_Util_nextIterator);
	assert(net_rizon_pyva_Util_getClassName);
	assert(net_rizon_pyva_Util_getAttribute);
	assert(net_rizon_pyva_Util_setAttribute);
	assert(net_rizon_pyva_Util_invokeConstructor);
	assert(net_rizon_pyva_Util_invoke);
	assert(net_rizon_pyva_Util_getCause);
	assert(net_rizon_pyva_Util_splitException);
	assert(net_rizon_pyva_Util_prettyException);
}

void JNI::Stop()
{
	java_lang_Object.Release();
	java_lang_Boolean.Release();
	java_lang_Integer.Release();
	java_lang_Double.Release();
	java_lang_Long.Release();
	java_lang_String.Release();
	java_lang_Throwable.Release();
	net_rizon_pyva_PyvaException.Release();
	net_rizon_pyva_Util.Release();
}

JNI::Env::Env() : attached(false), e(nullptr)
{
	if (vm->GetEnv(reinterpret_cast<void **>(&this->e), JNI_VERSION_1_6) != JNI_OK)
	{
		vm->AttachCurrentThread(reinterpret_cast<void **>(&this->e), nullptr);
		this->attached = true;
	}
}

JNI::Env::~Env()
{
	if (this->attached)
		vm->DetachCurrentThread();
}

JNIEnv *JNI::Env::GetEnv() const
{
	return this->e;
}

int JNI::Hash(jobject obj)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	return env->CallIntMethod(obj, java_lang_Object_hashCode);
}

JNI::String::String() : jstr(nullptr), cstr(nullptr)
{
}

JNI::String::String(jstring j) : jstr(j), cstr(nullptr)
{
}

JNI::String::String(const String &other)
{
	jstr = other.jstr;
	cstr = nullptr;
}

JNI::String::~String()
{
	if (cstr != nullptr)
	{
		JNI::Env jnienv;
		JNIEnv *env = jnienv.GetEnv();

		env->ReleaseStringUTFChars(jstr, cstr);
	}
}

JNI::String& JNI::String::operator=(const String &other)
{
	if (this != &other)
	{
		if (cstr != nullptr)
		{
			JNI::Env jnienv;
			JNIEnv *env = jnienv.GetEnv();

			env->ReleaseStringUTFChars(jstr, cstr);
			cstr = nullptr;
		}

		jstr = other.jstr;
	}
	return *this;
}

JNI::String::operator const char *()
{
	if (cstr == nullptr)
	{
		JNI::Env jnienv;
		JNIEnv *env = jnienv.GetEnv();

		cstr = env->GetStringUTFChars(jstr, nullptr);
	}

	return cstr;
}

JNI::String::operator std::string()
{
	const char *cstr = *this;
	return cstr ? cstr : "";
}

template<typename T>
JNI::LocalReference<T>::LocalReference(T j) : ref(j)
{
}

template<typename T>
JNI::LocalReference<T>::LocalReference(LocalReference<T> &&other)
{
	ref = other.ref;
	other.ref = nullptr;
}

template<typename T>
JNI::LocalReference<T>::~LocalReference()
{
	if (ref != nullptr)
	{
		JNI::Env jnienv;
		JNIEnv *env = jnienv.GetEnv();

		env->DeleteLocalRef(ref);
	}
}

template<typename T>
JNI::LocalReference<T>& JNI::LocalReference<T>::operator=(LocalReference<T> &&other)
{
	if (this != &other)
	{
		ref = other.ref;
		other.ref = nullptr;
	}
	return *this;
}

template<typename T>
JNI::LocalReference<T>::operator T() const
{
	return ref;
}

template class JNI::LocalReference<jobject>;
template class JNI::LocalReference<jstring>;

template<typename T>
JNI::StaticGlobalReference<T>::StaticGlobalReference() : ref(nullptr)
{
}

template<typename T>
JNI::StaticGlobalReference<T>::StaticGlobalReference(T j)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	ref = static_cast<T>(env->NewGlobalRef(j));
}

template<typename T>
JNI::StaticGlobalReference<T>::StaticGlobalReference(StaticGlobalReference<T> &&other)
{
	ref = other.ref;
	other.ref = nullptr;
}

template<typename T>
JNI::StaticGlobalReference<T>::~StaticGlobalReference()
{
	/* this is called from the actual JVM shutting down, which is way too late to Release() */
}

template<typename T>
void JNI::StaticGlobalReference<T>::Release()
{
	if (ref != nullptr)
	{
		JNI::Env jnienv;
		JNIEnv *env = jnienv.GetEnv();

		env->DeleteGlobalRef(ref);
		ref = nullptr;
	}
}

template<typename T>
JNI::StaticGlobalReference<T>& JNI::StaticGlobalReference<T>::operator=(StaticGlobalReference<T> &&other)
{
	if (this != &other)
	{
		Release();
		ref = other.ref;
		other.ref = nullptr;
	}
	return *this;
}

template<typename T>
JNI::StaticGlobalReference<T>::operator T() const
{
	return ref;
}

template class JNI::StaticGlobalReference<jobject>;
template class JNI::StaticGlobalReference<jstring>;

template<typename T>
JNI::GlobalReference<T>::GlobalReference(T ref) : JNI::StaticGlobalReference<T>(ref)
{
}

template<typename T>
JNI::GlobalReference<T>::~GlobalReference()
{
	this->Release();
}

template class JNI::GlobalReference<jobject>;
template class JNI::GlobalReference<jstring>;
