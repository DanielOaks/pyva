#include "python.hpp"
#include "pyva.h"
#include "pyva_object.hpp"

Thread::Mutex Pyva::interpreters_lock;
std::vector<Pyva *> Pyva::interpreters;

jobject to_java(PyObject *obj)
{
	if (obj == nullptr)
		return nullptr;

	if (IS_PYVA(obj))
	{
		PyvaObject *pv = reinterpret_cast<PyvaObject *>(obj);
		return pv->object;
	}

	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	if (PyTuple_Check(obj) || PyList_Check(obj))
	{
		Py_ssize_t length = PyTuple_Check(obj) ? PyTuple_Size(obj) : PyList_Size(obj);
		jclass listType = nullptr;
		jobject *jobs = new jobject[length];

		for (Py_ssize_t i = 0; i < length; ++i)
		{
			/* this is a borrowed reference */
			PyObject *o = PyTuple_Check(obj) ? PyTuple_GetItem(obj, i) : PyList_GetItem(obj, i);

			/* store object for next iteration */
			jobject obj = jobs[i] = to_java(o);

			if (obj == nullptr)
				continue;

			jclass cls = env->GetObjectClass(obj);

			if (listType == nullptr)
				listType = cls; // first class
			else if (!env->IsSameObject(listType, cls))
				listType = JNI::java_lang_Object; // mixed types, just use java.lang.Object
		}

		if (listType == nullptr)
			listType = JNI::java_lang_Object;

		jobjectArray array = env->NewObjectArray(length, listType, nullptr);
		for (Py_ssize_t i = 0; i < length; ++i)
		{
			env->SetObjectArrayElement(array, i, jobs[i]);
		}

		delete [] jobs;

		return array;
	}

#if PY_MAJOR_VERSION <= 2
	if (PyString_Check(obj))
	{
		const char *_value = PyString_AsString(obj);
		return env->NewStringUTF(_value);
	}

	if (PyUnicode_Check(obj))
	{
		Py_ssize_t len = PyUnicode_GET_SIZE(obj);
		Py_UNICODE *pchars = PyUnicode_AS_UNICODE(obj);

		Python::Reference string = PyUnicode_EncodeUTF8(pchars, len, NULL);
		if (!string)
			return nullptr;

		const char *_value = PyString_AsString(string);
		return env->NewStringUTF(_value);
	}
#else
	if (PyUnicode_Check(obj))
	{
		if (PyUnicode_READY(obj))
			return nullptr;

		const char *value = PyUnicode_AsUTF8(obj);
		if (value == nullptr)
			return nullptr;

		return env->NewStringUTF(value);
	}
#endif

	if (PyLong_CheckExact(obj))
	{
		long l = PyLong_AsLong(obj);
		return env->NewObject(JNI::java_lang_Long, JNI::java_lang_Long_init, l);
	}

#if PY_MAJOR_VERSION == 2
	if (PyInt_CheckExact(obj))
	{
		int i = PyInt_AsLong(obj);
		return env->NewObject(JNI::java_lang_Integer, JNI::java_lang_Integer_init, i);
	}
#endif

	if (PyFloat_CheckExact(obj))
	{
		double d = PyFloat_AsDouble(obj);
		return env->NewObject(JNI::java_lang_Double, JNI::java_lang_Double_init, d);
	}

	if (PyBool_Check(obj))
	{
		bool b = obj == Py_True;
		return env->NewObject(JNI::java_lang_Boolean, JNI::java_lang_Boolean_init, b);
	}

	return nullptr;
}

PyObject *to_python(jobject obj)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	if (obj == nullptr || env->IsSameObject(obj, nullptr))
		Py_RETURN_NONE;

	jboolean b;
	{
		Python::AllowThreads t;
		b = env->CallStaticBooleanMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_isArray, obj);
	}
	if (b)
	{
		jobjectArray args = static_cast<jobjectArray>(obj);
		jsize len = env->GetArrayLength(args);
		PyObject *py_args = PyTuple_New(len);

		if (py_args == nullptr)
			Py_RETURN_NONE;

		for (int i = 0; i < len; ++i)
		{
			jobject obj = env->GetObjectArrayElement(args, i);
			PyObject *pyobj = to_python(obj);

			/* Tuple takes ownership of pyobj */
			PyTuple_SetItem(py_args, i, pyobj);
		}

		return py_args;
	}

	if (env->IsInstanceOf(obj, JNI::java_lang_String))
	{
		JNI::String str = static_cast<jstring>(obj);
#if PY_MAJOR_VERSION >= 3
		return PyUnicode_FromString(str);
#else
		return PyString_FromString(str);
#endif
	}

	if (env->IsInstanceOf(obj, JNI::java_lang_Long))
	{
		long l = env->CallLongMethod(obj, JNI::java_lang_Long_longValue);
		return PyLong_FromLong(l);
	}

	if (env->IsInstanceOf(obj, JNI::java_lang_Integer))
	{
		int i = env->CallIntMethod(obj, JNI::java_lang_Integer_intValue);
#if PY_MAJOR_VERSION >= 3
		return PyLong_FromLong(i);
#else
		return PyInt_FromLong(i);
#endif
	}

	if (env->IsInstanceOf(obj, JNI::java_lang_Double))
	{
		double d = env->CallDoubleMethod(obj, JNI::java_lang_Double_doubleValue);
		return PyFloat_FromDouble(d);
	}

	if (env->IsInstanceOf(obj, JNI::java_lang_Boolean))
	{
		bool b = env->CallBooleanMethod(obj, JNI::java_lang_Boolean_booleanValue);
		return PyBool_FromLong(b);
	}

	Pyva *pyva = Pyva::Get();
	PyObject *py_object = pyva->IsCached(obj);
	if (py_object != nullptr)
	{
		Py_INCREF(py_object);
		return py_object;
	}

	PyvaObject *pyva_object;
	if (env->IsInstanceOf(obj, JNI::java_lang_Throwable))
	{
		pyva_object = PyObject_GC_New(PyvaObject, &PyvaExceptionType);
	}
	else
	{
		pyva_object = PyObject_GC_New(PyvaObject, &PyvaObjectType);
	}
	if (pyva_object == nullptr)
		Py_RETURN_NONE;

	pyva_object->dict = nullptr;
	pyva_object->object = env->NewWeakGlobalRef(obj);
	pyva_object->parent = nullptr;

	pyva->Cache(obj, reinterpret_cast<PyObject *>(pyva_object));

	PyObject_GC_Track(pyva_object);

	return reinterpret_cast<PyObject *>(pyva_object);
}

Pyva::Pyva(jobject p) : pyva(p)
{
	{
		/* Lock the main interpreter */
		Python::Lock global_lock;

		/* create a new subinterpreter. this doesn't require a current thread state, only that the GIL is set,
		 * so all it does on error is return nullptr
		 */
		state = Py_NewInterpreter();
		assert(state != nullptr);
	}
	/* unlock main interpreter */

	RegisterPyva();

	Python::Lock lock(this);

	char pathbuf[] = "path";
	Python::Reference pypath = PyUnicode_FromString(".");
	PyObject *py_sys = PySys_GetObject(pathbuf);
	PyList_Append(py_sys, pypath);

	if (PyErr_Occurred())
		PyErr_Print();
}

Pyva::~Pyva()
{
	/* interpreters lock must be held */
	assert(std::find(interpreters.begin(), interpreters.end(), this) == interpreters.end());

	/* restore the main thread context of this interpreter */
	PyEval_RestoreThread(state);

	/* Clear my cache */
	Clear();

#ifndef WIN32
	/* forcibly-ish abort native Python threads for this interpreter.
	 * If we don't, they will resume execution after the interpreter has
	 * been destroyed, and we die.
	 */
	PyInterpreterState *interpreter = state->interp;
	for (PyThreadState *p = interpreter->tstate_head, *next = p ? p->next : nullptr; p; p = next, next = p ? p->next : nullptr)
	{
		/* but don't free the main interpreter thread */
		if (p == state)
			continue;

		/* axe thread */
		pthread_cancel(p->thread_id);
		PyThreadState_Clear(p);
		PyThreadState_Delete(p);
	}
#endif

	/* Destroy the interpreter */
	Py_EndInterpreter(state);

	/* now this interprter is dead, and the global thread state is null,
	 * however we are still holding the GIL, so release it
	 */
	PyEval_ReleaseLock();
}

Pyva *Pyva::Get(jobject object)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	Thread::Lock interpreter_lock(interpreters_lock);

	/* find the Pyva instance associated with this net.rizon.pyva.Pyva instance */
	for (Pyva *p : interpreters)
		if (env->IsSameObject(object, p->pyva))
			return p;

	return nullptr;
}

Pyva *Pyva::Get()
{
	/* find the Pyva instance for the interpreter of the current thread state */
	PyThreadState *state = PyThreadState_GET();
	PyInterpreterState *interp = state->interp;

	Thread::Lock interpreter_lock(interpreters_lock);
	for (Pyva *p : interpreters)
		if (p->state->interp == interp)
			return p;

	/* trying to get a Pyva object for an interpreter we didn't create */
	assert(false);
	return nullptr;
}

JNIEXPORT void JNICALL Java_net_rizon_pyva_Pyva_start(JNIEnv *, jobject pyva)
{
	assert(Pyva::Get(pyva) == nullptr);

	/* All of these JVM methods are synchronized by the JVM, so another thread cannot be here with the same pyva object */
	Thread::Lock interpreter_lock(Pyva::interpreters_lock);
	Pyva::interpreters.push_back(new Pyva(pyva));
}

JNIEXPORT void JNICALL Java_net_rizon_pyva_Pyva_stop(JNIEnv *env, jobject pyva)
{
	Thread::Lock interpreter_lock(Pyva::interpreters_lock);

	for (auto it = Pyva::interpreters.begin(); it != Pyva::interpreters.end(); ++it)
	{
		Pyva *p = *it;

		if (env->IsSameObject(pyva, p->pyva))
		{
			Pyva::interpreters.erase(it);
			delete p;
			break;
		}
	}
}

JNIEXPORT void JNICALL Java_net_rizon_pyva_Pyva_addToSystemPath(JNIEnv *, jobject pyva, jstring path_name)
{
	Python::Lock lock(Pyva::Get(pyva));

	JNI::String path = path_name;
#if PY_MAJOR_VERSION >= 3
	Python::Reference pypath = PyUnicode_FromString(path);
#else
	Python::Reference pypath = PyString_FromString(path);
#endif

	char pathbuf[] = "path";
	PyObject *py_sys = PySys_GetObject(pathbuf);
	PyList_Append(py_sys, pypath);

	if (PyErr_Occurred())
		PyErr_Print();
}

JNIEXPORT jint JNICALL Java_net_rizon_pyva_Pyva_gc(JNIEnv *, jobject pyva)
{
	Pyva *p = Pyva::Get(pyva);
	Python::Lock pylock(p);
	return p->GC();
}

JNIEXPORT jobject JNICALL Java_net_rizon_pyva_Pyva_invoke(JNIEnv *env, jobject pyva, jstring module_name, jstring function_name, jobjectArray args)
{
	Python::Lock lock(Pyva::Get(pyva));

	Python::Reference pypyva = PyImport_ImportModule("pyva");
	if (pypyva == nullptr)
	{
		if (PyErr_Occurred())
			PyErr_Print();
		return nullptr;
	}

	Python::Reference invoke = PyObject_GetAttrString(pypyva, "invoke");
	if (invoke == nullptr)
	{
		if (PyErr_Occurred())
			PyErr_Print();
		return nullptr;
	}

	/* Tuple steals these references in PyTuple_SetItem() */
	PyObject *pymodname = to_python(module_name), *pyfuncname = to_python(function_name), *pyargs = to_python(args);

	Python::Reference targs = PyTuple_New(3);
	PyTuple_SetItem(targs, 0, pymodname);
	PyTuple_SetItem(targs, 1, pyfuncname);
	PyTuple_SetItem(targs, 2, pyargs);

	Python::Reference pyret = PyObject_CallObject(invoke, targs);

	if (!PyErr_Occurred())
	{
		return to_java(pyret);
	}

	/* An exception was thrown */

	Python::Reference type, value, traceback;
	PyErr_Fetch(type.Pointer(), value.Pointer(), traceback.Pointer()); /* This clears the exception */
	PyErr_NormalizeException(type.Pointer(), value.Pointer(), traceback.Pointer());

	jobject jexception = to_java(value);

	if (jexception != nullptr && env->IsInstanceOf(jexception, JNI::java_lang_Throwable))
	{
		/* Remove native frames from the stack trace because we're probably not interested in them */
		env->CallStaticVoidMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_prettyException, jexception);

		env->Throw(static_cast<jthrowable>(jexception));
	}
	else
	{
		/* PyErr_Restore steals references to the arguments for some reason */
		Py_INCREF(type);
		Py_INCREF(value);
		Py_INCREF(traceback);
		/* Restore the exception */
		PyErr_Restore(type, value, traceback);
		/* This is bad and means something in pyva.invoke() threw from the exception handler, so print a normal Python error */
		PyErr_Print();
	}

	return nullptr;
}

void Pyva::AddTracebackFrame(const std::string &file, const std::string &method, int line)
{
	Python::Reference py_globals;
	Python::TypedReference<PyCodeObject> py_code;
	Python::TypedReference<PyFrameObject> py_frame;

	py_globals = PyDict_New();
	if (!py_globals)
		return;

	py_code = PyCode_NewEmpty(file.c_str(), method.c_str(), line);
	if (!py_code)
		return;

	py_frame = PyFrame_New(PyThreadState_Get(), py_code, py_globals, nullptr);
	if (!py_frame)
		return;

	py_frame->f_lineno = line;
	PyTraceBack_Here(py_frame);
}

